var myApp = angular.module('myApp', [
  'ngRoute',
  'friendControllers'
]);

myApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  when('/register', {
    templateUrl: 'partials/register.html',
    controller: 'RegistrationController'
  }).
  when('/list', {
    templateUrl: 'partials/list.html',
    controller: 'ListController'
  }).
  when('/details/:itemId', {
    templateUrl: 'partials/details.html',
    controller: 'DetailsController'
  }).
  when('/welcome', {
    templateUrl: 'partials/welcome.html',
    controller: 'ListController'
  }).
  otherwise({
    redirectTo: '/list'
  });
}]);